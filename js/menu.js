var menuState = {

    create: function(){
        
        game.add.sprite(0, 0, "sky");
        var dude = game.add.sprite(game.world.width / 6, 350, 'dude');
        dude.frame = 4;
        dude.scale.setTo(2, 2);
        
        var titleLabel = game.add.text(game.world.width / 4, 300, 'PhaserJumper', {font: '65px ' + gameFont, fill: '#FFFFFF'});
        var startLabel = game.add.text(game.world.width / 4, 400, 'Press ENTER to start!', {font: '40px '+ gameFont, fill: '#FFFFFF'});
        
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        
        enterKey.onDown.addOnce(this.start, this);
    
    },
    
    start : function(){
     
        game.state.start('play');
        console.info('State has been changed: menuState -> playState');
        
    }

};