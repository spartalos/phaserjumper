var bootState = {

    create : function(){
    
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.state.start('load');
        console.info('State has changed: bootState -> loadState.');
    }

};