var playState = {
  
    platforms : null,
    star : null,

    player : null,
    cursors : null,
    double_jump : false,

    score : 0,
    scoreText : '',
    
    timer : 0,
    timerText : '',
    timerInterval : null,
    timerMax : 10,
    
    lives : null,

    game_is_on : false,
    start_text : '',
    
    create : function(){
        
        playState.cursors = game.input.keyboard.createCursorKeys();
        playState.cursors.up.onDown.add(playState.jump, this);
        
        game.add.sprite(0, 0, "sky");

        playState.platforms = game.add.group(); 
        playState.platforms.enableBody = true;

        playState.startGame();
        
    },
    
    update : function(){
        
        game.physics.arcade.collide(playState.player, playState.platforms);
        game.physics.arcade.collide(playState.star, playState.platforms);
        game.physics.arcade.overlap(playState.player, playState.star, playState.collectStars, null, this);

        playState.movement();

        if(playState.player.body.y > 550){
            playState.player.kill();
            playState.loseHeart();
            playState.createPlayer();
        }

        if(playState.double_jump === false && playState.player.body.touching.down)
            playState.double_jump = true;
        
    },
    
    
    startGame : function(){
        playState.star = null;
        playState.timerMax = 10;
		playState.score = 0;
        playState.createHUD();
        playState.createLedges();
        playState.createPlayer();
        playState.createStar();
        
        playState.timer = playState.timerMax;
        playState.timerInterval = setInterval(playState.setTimer, 1000);
    },

    createHUD : function(){
        playState.scoreText = game.add.text(16, 16, 'SCORE: 0', {font: '32px ' + gameFont, fill: '#FFF'});
        playState.timerText = game.add.text(350, 16, 'TIME: ' + playState.timerMax, {font: '32px ' + gameFont, fill: '#FFF'});

        playState.lives = game.add.group();
        playState.lives.create(700, 16, 'heart');
        playState.lives.create(724, 16, 'heart');
        playState.lives.create(748, 16, 'heart');
    },

    createLedges : function(){
        for(var i = 1; i <= 6; i++){
            playState.createLedge((Math.random() * (game.world.width - 200)), i * 90);
        }
    },

    createLedge : function(x, y){
        var ledge = playState.platforms.create(x, y, 'ground');
        ledge.body.immovable = true;
       // ledge.scale.setTo(0.5, 1);
    },

    createPlayer : function(){
        playState.player = game.add.sprite(playState.platforms.children[playState.platforms.countLiving()-1].body.x + 50,
                                 playState.platforms.children[playState.platforms.countLiving()-1].body.y - 50, 'dude');
        game.physics.arcade.enable(playState.player);
        playState.player.body.bounce.y = 0.2;
        playState.player.body.gravity.y = 300;
        playState.player.body.collideWorldBounds = true;
        playState.player.animations.add('left', [0, 1, 2, 3], 10, true); 
        playState.player.animations.add('right', [5, 6, 7, 8], 10, true);
    },

    createStar : function(){

        var platform_num = (Math.random() * (playState.platforms.countLiving() - 1)).toFixed();
        var starX = ((Math.random() * playState.platforms.children[platform_num].body.width) +
                                    playState.platforms.children[platform_num].body.x);
        var starY =  playState.platforms.children[platform_num].body.y - 50;

        if(playState.star === null){
            
            playState.star = game.add.sprite(starX, starY, "star");

            game.physics.arcade.enable(playState.star);
            playState.star.body.gravity.y = 200;
            playState.star.body.bounce.y = 0.7 + Math.random() * 0.2;
            
        }else{
            
            playState.star.body.x = starX;   
            playState.star.body.y = starY;   
        }
    
    },

    collectStars : function(player, star){

        playState.score += 10;
        playState.scoreText.text = "SCORE: " + playState.score;
        playState.timer = playState.timerMax;
        playState.timerText.text = "TIME: " + playState.timer;

        if(playState.score % 100 == 0){
            playState.platforms.removeAll();
            playState.createLedges();
            //Making some difficulty :P
            if(playState.timerMax > 5)
                playState.timerMax--;
        }

        playState.createStar();
    },

    movement : function(){

        playState.player.body.velocity.x = 0;

        if(playState.cursors.left.isDown){

            playState.player.body.velocity.x = -150;
            playState.player.animations.play('left');

        }else if(playState.cursors.right.isDown){

            playState.player.body.velocity.x = 150;
            playState.player.animations.play('right');

        }else{
            playState.player.animations.stop();
            playState.player.frame = 4;
        }

    },

    jump : function(){

        if(playState.cursors.up.isDown){

            if(playState.player.body.touching.down){

                playState.player.body.velocity.y = -250;

            }else if(playState.double_jump){
                    playState.player.body.velocity.y = -250;
                    playState.double_jump = false;
            }

        }
    },

    loseHeart : function(){
        
         if(playState.lives.countLiving() - 1 > 0){
             
            playState.lives.children[playState.lives.countLiving() - 1].kill();
            playState.timer = playState.timerMax;
            playState.timerText.text = "TIME: " + playState.timer;
            
         }else{
           
            clearInterval(playState.timerInterval);
             
            game.add.text(200, 268, "GAME OVER", {font: '64px ' + gameFont, fill: "#FFF"});
            game.add.text(210, 350, "Press ENTER!", {font: '40px ' + gameFont, fill: "#FFF"});
            
            var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            enterKey.onDown.addOnce(playState.gameOver, this);
            
            game.gamePaused();
             
         }
    },
    
    gameOver : function(){
        game.gameResumed();
        game.state.start('menu');
        console.info('state has been changed: playState -> menuState');
    },
    
    setTimer : function(){
    
        playState.timer--;

        if(playState.timer === 0){

            //because it's not changing the star position neither...
            playState.star.kill();
            playState.star = null;
            
            playState.createStar();
            playState.loseHeart();

        }
        
        playState.timerText.text = "TIME: " + playState.timer;

    }

};