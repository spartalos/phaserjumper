var loadState = {

    preload : function(){
      
        game.load.image("sky", "asset/sky.png");
	    game.load.image("ground", "asset/platform.png");
	    game.load.image("star", "asset/star.png");
        game.load.image("heart", "asset/heart.png");
	    game.load.spritesheet("dude", "asset/dude.png", 32, 48);
        
    },
    
    create : function(){
     
        game.state.start('menu');
        console.info('State has changed: loadState -> menuState.');
        
    }

};