### Summary ###

My first platformer game called PhaserJumper.
Version: 0.2

### How to run ###

PC:

1. Download wamp server.
2. Install wamp server.
3. Put game into {wamp_server_install_path}/www directory.
4. Run wamp server.
5. Run game from browser by the following URL: localhost/phasertutorial.

### Who do I talk to? ###

You can reach me on my email:
spartalos16@gmail.com